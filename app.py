from flask import Flask, render_template, request
import copy
import requests
import requests_cache
app = Flask(__name__)
requests_cache.install_cache('demo_cache')

@app.route('/', methods=['GET', 'POST'])
def submit():
  if request.method == 'POST':
    city = request.form['city']
    checkin = request.form['checkin']
    checkout = request.form['checkout']
    table_data = submit_request(city, checkin, checkout)
    print(table_data)
    return render_template('demo.html', data = table_data)
  return render_template('demo.html')

def submit_request(city, checkin, checkout):
  url = 'https://experimentation.snaptravel.com/interview/hotels'
  generic_request = {
    'city': city,
    'checkin': checkin,
    'checkout': checkout,
    'provider': 'snaptravel'
  }
  snap_travel_request = generic_request
  hotels_request = copy.deepcopy(generic_request)
  hotels_request['provider'] = 'hotels'

  snap_travel_data = requests.post(url, data = snap_travel_request)
  hotels_data = requests.post(url, data = hotels_request)

  if city == 'Testing':
    # Test Data
    return compare_and_filter([{
        'id' : 12,
        'hotel_name' : 'Center Hilton',
        'num_reviews' : 209,
        'address' : '12 Wall Street, Very Large City',
        'num_stars' : 4,
        'amenities' : ['Wi-Fi', 'Parking'],
        'image_url' : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
        'price' : 132.11
      },
      {
        'id' : 16,
        'hotel_name' : 'Grand Hyatt',
        'num_reviews' : 209,
        'address' : '12 Wall Street, Very Large City',
        'num_stars' : 4,
        'amenities' : ['Wi-Fi', 'Parking'],
        'image_url' : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
        'price' : 137.11
      }
    ], [{
        'id' : 12,
        'hotel_name' : 'Center Hilton',
        'num_reviews' : 209,
        'address' : '12 Wall Street, Very Large City',
        'num_stars' : 4,
        'amenities' : ['Wi-Fi', 'Parking'],
        'image_url' : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
        'price' : 122.11
      },
      {
        'id' : 27,
        'hotel_name' : 'Royal York',
        'num_reviews' : 209,
        'address' : '12 Wall Street, Very Large City',
        'num_stars' : 4,
        'amenities' : ['Wi-Fi', 'Parking'],
        'image_url' : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
        'price' : 117.11
      },
      {
        'id' : 16,
        'hotel_name' : 'Grand Hyatt',
        'num_reviews' : 209,
        'address' : '12 Wall Street, Very Large City',
        'num_stars' : 4,
        'amenities' : ['Wi-Fi', 'Parking'],
        'image_url' : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
        'price' : 137.11
      }
    ])
  else:
    # Real Request
    return compare_and_filter([snap_travel_data], hotels_data)

def compare_and_filter(snap_travel_data, hotels_data):
  filtered_set = []
  hotel_map = {}

  for hotel in snap_travel_data:
    hotel_map[hotel['id']] = hotel

  for hotel in hotels_data:
    if hotel['id'] in hotel_map:
      filtered_hotel = hotel_map[hotel['id']]
      filtered_hotel['hotels.com price'] = hotel['price']
      filtered_set.append(filtered_hotel)

  return filtered_set
